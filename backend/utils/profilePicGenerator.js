const profilePictureUrlGenerator = async (username, gender) => {
    if (gender === "male")
        return `https://avatar.iran.liara.run/public/boy?username=${username}`;

    if (gender === "female")
        return `https://avatar.iran.liara.run/public/girl?username=${username}`;

    return "";
};

export default profilePictureUrlGenerator;
