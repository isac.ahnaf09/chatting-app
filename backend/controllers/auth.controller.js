import User from "../models/user.model.js";
import bcrypt from "bcryptjs";
import profilePictureUrlGenerator from "../utils/profilePicGenerator.js";
import generateTokenAndSetCookie from "../utils/generateToken.js";

export const signup = async (req, res) => {
    try {
        const { fullName, username, password, confirmPassword, gender } =
            req.body;

        if (password !== confirmPassword) {
            return res.status(400).json({
                message: "Passwords do not match",
            });
        }

        const user = await User.findOne({ username });

        if (user) {
            return res.status(400).json({
                message: "username is already exist",
            });
        }

        //HASH PASSWORD HERE
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);

        const newUser = new User({
            fullName,
            username,
            password: hashedPassword,
            gender,
            profilePic: await profilePictureUrlGenerator(username, gender),
        });

        if (newUser) {
            //generate token
            generateTokenAndSetCookie(newUser._id, res);

            //save to database
            await newUser.save();

            return res.status(201).json({
                _id: newUser._id,
                fullName: newUser.fullName,
                gender: newUser.gender,
                profilePic: newUser.profilePic,
            });
        } else {
            return res.status(400).json({
                message: "invalid user data",
            });
        }
    } catch (error) {
        console.log("Error at signup controller : ", error.message);
        return res.status(500).json({
            message: "internal server error",
        });
    }
};

export const login = async (req, res) => {
    try {
        const { username, password } = req.body;

        const user = await User.findOne({ username });
        const isPasswordCorrect = await bcrypt.compare(
            password,
            user?.password || ""
        );

        if (!user) {
            return res.status(400).json({ message: "username is not exist" });
        }

        if (!isPasswordCorrect) {
            return res
                .status(400)
                .json({ message: "invalid username or password" });
        }

        generateTokenAndSetCookie(user._id, res);

        return res.status(200).json({
            _id: user._id,
            fullName: user.fullName,
            username: user.username,
            profilePic: user.profilePic,
        });
    } catch (error) {
        console.log("Error at login controller : ", error.message);
        return res.status(500).json({
            message: "internal server error",
        });
    }
};

export const logout = async (req, res) => {
    try {
        res.cookie("jwt", "", { maxAge: 0 });

        return res.status(200).json({ message: "logged out successfully" });
    } catch (error) {
        console.log("Error at logout controller : ", error.message);
        return res.status(500).json({
            message: "internal server error",
        });
    }
};
