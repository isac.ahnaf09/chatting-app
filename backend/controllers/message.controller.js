import Conversation from "../models/conversation.model.js";
import Message from "../models/message.model.js";
import { getReceiverSocketId, io } from "../socket/socket.js";

export const sendMessage = async (req, res) => {
    try {
        const { message } = req.body;
        const { id: receiverId } = req.params;
        const senderId = req.user._id;

        let conversation = await Conversation.findOne({
            participants: {
                $all: [senderId, receiverId],
            },
        });

        if (!conversation) {
            conversation = await Conversation.create({
                participants: [senderId, receiverId],
            });
        }

        const newMessage = await Message.create({
            senderId,
            receiverId,
            message,
        });

        if (newMessage) {
            conversation.messages.push(newMessage._id);
        }

        await Promise.all([conversation.save(), newMessage.save()]);

        //SOCKET IO FUNCTION HERE
        const userSocketId = getReceiverSocketId(receiverId);

        if (userSocketId) {
            io.to(userSocketId).emit("newMessage", newMessage);
        }

        res.status(201).json(newMessage);
    } catch (error) {
        console.log("error at sendMessage controller : " + error.message);
        return res.status(500).json({
            message: "internal server error",
        });
    }
};

export const getMessages = async (req, res, next) => {
    try {
        const { id: userToChatWith } = req.params;
        const senderId = req.user._id;

        const conversation = await Conversation.findOne({
            participants: {
                $all: [userToChatWith, senderId],
            },
        }).populate("messages");

        //return empty array if no conversation
        if (!conversation) return res.status(200).json([]);

        const messages = conversation.messages;

        return res.status(200).json(messages);
    } catch (error) {
        console.log("error at getMessages controller : " + error.message);
        return res.status(500).json({
            message: "internal server error",
        });
    }
};
