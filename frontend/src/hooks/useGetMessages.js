import { useEffect, useState } from "react";
import useConversation from "../store/useConversation";
import toast from "react-hot-toast";

function useGetMessages() {
    const [loading, setLoading] = useState(false);
    const { messages, setMessages, selectedConversation } = useConversation();

    useEffect(() => {
        const getMessage = async () => {
            try {
                setLoading(true);

                const response = await fetch(
                    `/api/message/${selectedConversation?._id}`
                );

                console.log("response", response);

                const data = await response.json();
                console.log("response.data", data);

                if (response.status !== 200) return toast.error(data.message);

                if (data.error) throw new Error(data.error);

                setMessages(data);
            } catch (error) {
                console.error(error.message);
                toast.error("internal server error");
            } finally {
                setLoading(false);
            }
        };

        if (selectedConversation?._id) {
            getMessage();
        }
    }, [selectedConversation, setMessages]);

    return {
        loading,
        messages,
    };
}

export default useGetMessages;
