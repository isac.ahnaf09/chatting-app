import { useState } from "react";
import toast from "react-hot-toast";
import useConversation from "../store/useConversation";

function useSendMessage() {
    const [loading, setLoading] = useState(false);
    const { messages, setMessages, selectedConversation } = useConversation();

    const sendMessage = async (message) => {
        try {
            setLoading(true);

            const response = await fetch(
                `/api/message/send/${selectedConversation?._id}`,
                {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({ message }),
                }
            );

            const data = await response.json();

            if (response.status !== 201) return toast.error(data.message);

            if (data.error) throw new Error(data.error);

            setMessages([...messages, data]);
        } catch (error) {
            console.error(error.message);
            toast.error("internal server error");
        } finally {
            setLoading(false);
        }
    };

    return { loading, sendMessage };
}

export default useSendMessage;
