import { useState } from "react";
import { useAuthContext } from "../context/AuthContext";
import toast from "react-hot-toast";

function useLogin() {
    const [loading, setLoading] = useState(false);
    const { setAuthUser } = useAuthContext();

    const login = async ({ username, password }) => {
        try {
            const isValid = handleInputsError({ username, password });

            if (!isValid) return;

            const response = await fetch("/api/auth/login", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ username, password }),
            });

            const data = await response.json();

            console.log(response);

            if (response.status !== 200) return toast.error(data.message);

            if (data.error) throw new Error(data.error);

            localStorage.setItem("chat-app", JSON.stringify(data));
            setAuthUser(data);

            return toast.success("logged in successfully");
        } catch (error) {
            console.error(error.message);
            toast.error("internal server error");
        } finally {
            setLoading(false);
        }
    };

    return { loading, login };
}

const handleInputsError = ({ username, password }) => {
    if (!username || !password) {
        toast.error("please, fill in all fields");
        return false;
    }

    return true;
};

export default useLogin;
