import { useEffect } from "react";
import { useSocketContext } from "../context/SocketContext";
import useConversation from "../store/useConversation";
import notificationSound from "../assets/sound/pengumuman.mp3";

const useListenMessages = () => {
    const { socket } = useSocketContext();
    const { messages, setMessages, selectedConversation } = useConversation();
    console.log("here");

    useEffect(() => {
        socket?.on("newMessage", (newMessage) => {
            newMessage.shake = true;
            const audio = new Audio(notificationSound);
            audio.play();
            console.log("newMessage", newMessage);

            if (selectedConversation?._id === newMessage.senderId) {
                setMessages([...messages, newMessage]);
            }
        });

        return () => socket.off("newMessage");
    }, [socket, messages, setMessages]);
};

export default useListenMessages;
