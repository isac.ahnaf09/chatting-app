import { useState } from "react";
import toast from "react-hot-toast";
import { useAuthContext } from "../context/AuthContext";

function useSignup() {
    const [loading, setLoading] = useState(false);
    const { setAuthUser } = useAuthContext();

    const signup = async (userData) => {
        const { fullName, username, password, confirmPassword, gender } = userData;
        const isValid = handleInputsError({
            fullName,
            username,
            password,
            confirmPassword,
            gender,
        });

        if (!isValid) {
            return;
        }

        setLoading(true);
        try {
            const response = await fetch(`/api/auth/signup`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(userData),
            });

            const data = await response.json();

            if (data.error) throw new Error(data.error);

            localStorage.setItem("chat-app", JSON.stringify(data));

            setAuthUser(data);

            return toast.success("user created successfully");
        } catch (error) {
            console.error("error", error.message);
            toast.error("internal server error");
        } finally {
            setLoading(false);
        }
    };

    return { loading, signup };
}

export default useSignup;

const handleInputsError = ({ fullName, username, password, confirmPassword, gender }) => {
    if (!fullName || !username || !password || !confirmPassword || !gender) {
        toast.error("please fill in all fields");
        return false;
    }

    if (password !== confirmPassword) {
        toast.error("passwords do not match");
        return false;
    }

    return true;
};
