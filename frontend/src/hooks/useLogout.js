import { useState } from "react";
import toast from "react-hot-toast";
import { useAuthContext } from "../context/AuthContext";

function useLogout() {
    const [loading, setLoading] = useState(false);
    const { setAuthUser } = useAuthContext();

    const logout = async () => {
        try {
            setLoading(true);

            const response = await fetch("/api/auth/logout", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
            });

            const data = await response.json();

            if (data.error) throw new Error(data.error);

            localStorage.removeItem("chat-app");

            setAuthUser(null);
        } catch (error) {
            console.error("error", error.message);
            return toast.error("internal server error");
        } finally {
            setLoading(false);
        }
    };

    return { loading, logout };
}

export default useLogout;
