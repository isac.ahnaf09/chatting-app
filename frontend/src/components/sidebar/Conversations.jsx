import useGetConversations from "../../hooks/useGetConversations";
import { getRandomEmoji } from "../../utils/generateRandomEmoji";
import Conversation from "./Conversation";

function Conversations() {
    const { loading, conversations } = useGetConversations();

    console.log("consversation : ", conversations);

    return (
        <div className="py-2 flex flex-col overflow-auto">
            {loading ? (
                <span className="loading loading-spinner mx-auto"></span>
            ) : (
                conversations &&
                conversations.map((conversation, index) => {
                    return (
                        <Conversation
                            key={index}
                            conversation={conversation}
                            emoji={getRandomEmoji()}
                            isLastIndex={index === conversations.length - 1}
                        />
                    );
                })
            )}
        </div>
    );
}

export default Conversations;
