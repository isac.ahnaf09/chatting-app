import PropsTypes from "prop-types";
import useConversation from "../../store/useConversation";
import { useSocketContext } from "../../context/SocketContext";

function Conversation({ conversation, emoji, isLastIndex }) {
    const { _id, fullName, profilePic } = conversation;
    const { selectedConversation, setSelectedConversation } = useConversation();
    const { onlineUsers } = useSocketContext();

    const isSelected = _id === selectedConversation?._id;
    const isUserOnlineClass = onlineUsers.includes(_id) ? "online" : "";

    const handleSelectConversation = () => {
        setSelectedConversation(conversation);
    };

    return (
        <>
            <div
                className={`flex gap-2 items-center hover:bg-sky-500 rounded p-2 py-1 cursor-pointer ${
                    isSelected ? "bg-sky-500" : ""
                }`}
                onClick={handleSelectConversation}
            >
                <div className={`avatar ${isUserOnlineClass}`}>
                    <div className="w-12 rounded-full">
                        <img src={profilePic} alt="user avatar" />
                    </div>
                </div>

                <div className="flex flex-col flex-1">
                    <div className="flex gap-3 justify-between">
                        <p className="font-bold text-gray-200">{fullName}</p>
                        <span className="text-xl">{emoji}</span>
                    </div>
                </div>
            </div>

            {isLastIndex ? null : <div className="divider my-0 py-0 h-1" />}
        </>
    );
}

export default Conversation;

Conversation.propTypes = {
    conversation: PropsTypes.object.isRequired,
    emoji: PropsTypes.string,
    isLastIndex: PropsTypes.bool,
};
