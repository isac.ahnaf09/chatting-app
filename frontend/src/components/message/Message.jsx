import PropsTypes from "prop-types";
import { useAuthContext } from "../../context/AuthContext";
import useConversation from "../../store/useConversation";
import { extractTime } from "../../utils/extractTime";

function Message({ message }) {
    const { authUser } = useAuthContext();
    const { selectedConversation } = useConversation();

    const isFromMe = authUser?._id === message?.senderId;
    const chatClass = isFromMe ? "chat-end" : "chat-start";
    const profilePic = isFromMe
        ? authUser?.profilePic
        : selectedConversation?.profilePic;
    const bubbleChatBg = isFromMe ? "bg-sky-500" : "";
    const formattedTime = extractTime(message.createdAt);
    const shakeClass = message.shake ? "shake" : "";
    if (shakeClass) console.log("newMessage", message);
    console.log("newMessage", message);

    return (
        <div className={`chat ${chatClass}`}>
            <div className="chat-image avatar">
                <div className="w-10 rounded-full">
                    <img src={profilePic || ""} alt="user avatar" />
                </div>
            </div>

            <div
                className={`chat-bubble text-white ${bubbleChatBg} ${shakeClass}`}
            >
                {message?.message}
            </div>
            <div className="chat-footer opacity-50 text-xs flex gap-1 items-center">
                {formattedTime}
            </div>
        </div>
    );
}

export default Message;

Message.propTypes = {
    message: PropsTypes.object.isRequired,
};
