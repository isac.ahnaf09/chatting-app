import MessageContainer from "../../components/message/MessageContainer";
import SideBar from "../../components/sidebar/SideBar";

function Home() {
    const api = import.meta.env.VITE_API_KEY;

    console.log("here", api);
    return (
        <div className="flex w-1/2W sm:h-[450px] md:h-[720px] rounded-lg overflow-hidden bg-gray-400 bg-clip-padding backdrop-filter backdrop-blur-lg bg-opacity-0">
            <SideBar />

            <MessageContainer />
        </div>
    );
}

export default Home;
