import PropsTypes from "prop-types";

function GenderCheckBox({ handleOnSelect, selectedGender }) {
    return (
        <div className="flex">
            <div className="form-control">
                <label className={`label gap-2 cursor-pointer`}>
                    <span className="label-text">Male</span>
                    <input
                        type="checkbox"
                        className="checkbox border-slate-900"
                        checked={selectedGender === "male"}
                        onChange={() => handleOnSelect("male")}
                    />
                </label>
            </div>

            <div className="form-control">
                <label className={`label gap-2 cursor-pointer`}>
                    <span className="label-text">Female</span>
                    <input
                        type="checkbox"
                        className="checkbox border-slate-900"
                        checked={selectedGender === "female"}
                        onChange={() => handleOnSelect("female")}
                    />
                </label>
            </div>
        </div>
    );
}

GenderCheckBox.propTypes = {
    handleOnSelect: PropsTypes.func.isRequired,
    selectedGender: PropsTypes.string.isRequired,
};

export default GenderCheckBox;
