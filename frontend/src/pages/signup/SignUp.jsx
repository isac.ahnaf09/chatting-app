import { Link } from "react-router-dom";
import GenderCheckBox from "./GenderCheckBox";
import { useState } from "react";
import useSignup from "../../hooks/useSignup";

function SignUp() {
    const [userData, setUserData] = useState({
        fullName: "",
        username: "",
        password: "",
        confirmPassword: "",
        gender: "",
    });
    const { loading, signup } = useSignup(userData);

    const handleOnChange = (e) => {
        const { name, value } = e.target;

        setUserData((prevData) => {
            return {
                ...prevData,
                [name]: value,
            };
        });
    };

    const handleCheckBoxGender = (gender) => {
        setUserData({ ...userData, gender });
    };
    const handleOnSubmit = async (e) => {
        e.preventDefault();

        await signup(userData);
    };

    return (
        <div className="flex flex-col items-center justify-center min-w-96 mx-auto">
            <div className="w-full p-6 rounded-lg shadow-md bg-gray-400 bg-clip-padding backdrop-filter backdrop-blur-lg bg-opacity-0">
                <h1 className="text-3xl font-semibold text-center text-gray-300">
                    Sign Up <span className="text-blue-500"> Yaggie App</span>
                </h1>

                <div className="mt-4">
                    <form onSubmit={handleOnSubmit}>
                        <div>
                            <label className="label p-2">
                                <span className="text-base label-text">Full Name</span>
                            </label>

                            <input
                                type="text"
                                name="fullName"
                                onChange={handleOnChange}
                                value={userData.fullName}
                                placeholder="John Doe"
                                className="w-full input input-bordered  h-10"
                            />
                        </div>

                        <div>
                            <label className="label p-2 ">
                                <span className="text-base label-text">Username</span>
                            </label>

                            <input
                                type="text"
                                name="username"
                                onChange={handleOnChange}
                                value={userData.username}
                                placeholder="johndoe"
                                className="w-full input input-bordered h-10"
                            />
                        </div>

                        <div>
                            <label className="label">
                                <span className="text-base label-text">Password</span>
                            </label>

                            <input
                                type="password"
                                name="password"
                                onChange={handleOnChange}
                                value={userData.password}
                                placeholder="Enter Password"
                                className="w-full input input-bordered h-10"
                            />
                        </div>

                        <div>
                            <label className="label">
                                <span className="text-base label-text">Confirm Password</span>
                            </label>
                            <input
                                type="password"
                                name="confirmPassword"
                                onChange={handleOnChange}
                                value={userData.confirmPassword}
                                placeholder="Confirm Password"
                                className="w-full input input-bordered h-10"
                            />
                        </div>

                        <GenderCheckBox handleOnSelect={handleCheckBoxGender} selectedGender={userData.gender} />

                        <div className="mb-1 flex items-center justify-start">
                            <Link
                                to={"/login"}
                                className="text-sm hover:underline hover:text-blue-600 mt-2 inline-block"
                            >
                                Already have an account?
                            </Link>
                        </div>

                        <div>
                            <button className="btn btn-block btn-sm mt-2 border border-slate-700" disabled={loading}>
                                {loading ? <span className="loading loading-spinner"></span> : "Sign Up"}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SignUp;
