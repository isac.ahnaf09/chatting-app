import { Link } from "react-router-dom";
import useLogin from "../../hooks/useLogin";
import { useState } from "react";

function Login() {
    const [userCredential, setUserCredential] = useState({
        username: "",
        password: "",
    });
    const { loading, login } = useLogin();

    const handleOnChange = (e) => {
        const { name, value } = e.target;

        setUserCredential((prevData) => {
            return {
                ...prevData,
                [name]: value,
            };
        });
    };

    const handleOnSubmit = async (e) => {
        e.preventDefault();

        await login(userCredential);
    };

    return (
        <div className="flex flex-col items-center justify-center min-w-96 mx-auto">
            <div className="w-full p-6 rounded-lg shadow-md bg-gray-800 bg-clip-padding backdrop-filter backdrop-blur-lg bg-opacity-0">
                <h1 className="text-3xl text-grey-300 font-semibold text-center">
                    LOGIN <span className="text-blue-500">Yaggie App</span>
                </h1>

                <div className="mt-3">
                    <form onSubmit={handleOnSubmit}>
                        <div>
                            <label className="label p-2">
                                <span className="text-base label-text">Username</span>
                            </label>
                            <input
                                type="text"
                                name="username"
                                value={userCredential.username}
                                onChange={handleOnChange}
                                placeholder="Enter username"
                                className="w-full input input-bordered h-10"
                            />
                        </div>

                        <div>
                            <label className="label">
                                <span className="text-base label-text">Password</span>
                            </label>
                            <input
                                type="password"
                                name="password"
                                value={userCredential.password}
                                onChange={handleOnChange}
                                placeholder="Enter Password"
                                className="w-full input input-bordered h-10"
                            />
                        </div>

                        <div className="mb-1 flex items-center justify-start">
                            <Link
                                to={"/signup"}
                                className="text-sm  hover:underline hover:text-blue-600 mt-2 inline-block"
                            >
                                {"Don't"} have an account?
                            </Link>
                        </div>

                        <div>
                            <button className="btn btn-block btn-sm mt-2" disabled={loading}>
                                {loading ? <span className="loading loading-spinner "></span> : "Login"}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Login;
